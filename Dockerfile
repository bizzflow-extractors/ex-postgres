FROM python:3.10-bullseye

LABEL com.bizztreat.type="Extractor"
LABEL com.bizztreat.purpose="Bizzflow"
LABEL com.bizztreat.component="ex-postgres"
LABEL com.bizztreat.title="PostgresSQL Extractor"

ADD requirements.txt ./

RUN pip3 install -r requirements.txt

VOLUME /data/out/tables
VOLUME /config

ADD src/ /code

WORKDIR /code

ENTRYPOINT ["python", "-u", "main.py"]
