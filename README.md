# PostgresSQL Extrator

## Description
Extractor for PostgresSQL databases. 
##### What it is used for?
Extracting the data from any PostgresSQL Database to the specific folder on the local machine.
##### What the code does?
The code - especially main.py file does:
1. Load the config file, where are all neccessary information for the extractor.
2. Create the connection between the machine, where is the script running and the remote database.
3. Ask for the specific data and save them to a specific folder on the local machine as CSV (everything is specified in the config.json file - read on).

## Requirements
* python3.5 or higher 

## How to use it?

#### Local use
1) Create or modify the config.json file (see example `config-sample.json`):
	* config.json =  configuration:
    	- config file path: /config/config.json
        - check that the file is the corrent folder, if not create it
    	- before running the script fill the config.json file:
        	- all fields are required
        	- field "query" options: could contain valid sql selects
            
2) Run the /scr/main.py
      ```sh
      python3 main.py
      ```
