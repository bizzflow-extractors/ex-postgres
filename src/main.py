#!/usr/bin/env python3

### Imports ###
from typing import Iterable, Optional
import psycopg2
import sys
import csv
import os
import json
import logging
from retry_helper import RetryManager
from base64 import b64encode

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

if len(sys.argv) == 2:
    config_path = sys.argv[1]
else:
    config_path = "/config/config.json"


def _memtostr(mem) -> str:
    if isinstance(mem, memoryview):
        return b64encode(bytes(mem)).decode("ascii")
    return mem


def _convert_bytes(iter: Iterable) -> Iterable:
    for line in iter:
        yield (_memtostr(column) for column in line)


class Extractor:
    def __init__(
        self,
        user: str,
        password: str,
        host: str,
        database: str,
        port: int,
        batch_size: Optional[int] = None,
        max_retries=10,
        retry_waiting_time=10,
    ):
        self.user = user
        self.password = password
        self.host = host
        self.database = database
        self.port = port
        self.batch_size = batch_size or 10000
        self._connection = None
        self.max_retries = max_retries
        self.retry_waiting_time = retry_waiting_time

    @property
    def connection(self):
        if self._connection is None:
            with RetryManager(
                max_attempts=self.max_retries,
                wait_seconds=self.retry_waiting_time,
                exceptions=(psycopg2.InternalError, psycopg2.OperationalError),
            ) as retry:
                while retry:
                    with retry.attempt:
                        self._connection = psycopg2.connect(
                            host=self.host,
                            dbname=self.database,
                            user=self.user,
                            password=self.password,
                            port=self.port,
                        )
        return self._connection

    def close_connection(self):
        if self._connection is None:
            return
        self._connection.close()
        self._connection = None

    def query(self, query: str, output: str):
        logger.info("Running query: %s", query)
        with RetryManager(
            max_attempts=self.max_retries,
            wait_seconds=self.retry_waiting_time,
            exceptions=(psycopg2.InternalError, psycopg2.OperationalError),
            reset_func=self.close_connection,
        ) as retry:
            while retry:
                with retry.attempt:
                    cursor_name: str = b64encode(output.encode("utf-8")).decode("ascii")
                    cursor = self.connection.cursor(name=cursor_name)
                    cursor.execute(query)
                    # funny hack, server-side cursors have no description unless first row is returned
                    first_row = cursor.fetchone()
                    fieldnames = [column[0] for column in cursor.description]
                    logger.info("Fieldnames: %s", ", ".join(fieldnames))
                    with open(output, "w", newline="", encoding="utf-8") as fid:
                        writer = csv.writer(fid, dialect=csv.unix_dialect)
                        writer.writerow(fieldnames)
                        if first_row:
                            writer.writerows(_convert_bytes([first_row]))
                            while True:
                                result = cursor.fetchmany(self.batch_size)
                                if not result:
                                    break
                                writer.writerows(_convert_bytes(result))
                    cursor.close()


def main():
    if not os.path.exists(config_path):
        raise Exception("Configuration not specified")

    with open(config_path) as conf_file:
        conf = json.load(conf_file)

    output_dir = conf.get("output_directory", "/data/out/tables")

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    port = int(conf.get("port", 5432))
    batch_size = conf.get("batch_size")
    client = Extractor(
        user=conf["user"],
        password=conf["password"],
        host=conf["host"],
        database=conf["database"],
        port=port,
        batch_size=batch_size,
        max_retries=conf.get("max_retries", 10),
        retry_waiting_time=conf.get("retry_waiting_time", 10),
    )
    for table, query in conf.get("query", {}).items():
        fname = os.path.join(output_dir, f"{table}.csv")
        client.query(query, fname)


if __name__ == "__main__":
    main()
